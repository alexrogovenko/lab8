Alexandra Rogovenko qualification model

English and communication

Read software requirements and technical design documents (B);
Write status updates and questions for tasks (C);
Write source code and commit messages (B).

Software development

Setup and configure personal local development environment and tools (B);
Write quality source code according to the organizational guidelines and industry best practices and submit code to version control system following organizational policies (B);
Implement production-ready standard software features according to the requirements, task definitions and technical design documentation (C);
Troubleshoot and resolve software defects (B);
Provide status updates about the work using task management system (C).

Working in team

Work in the team according to the standard process models (C).

My studying goals:
- Improve all my skills to advanced level
- Gain new skills required for junior qualification
- Learn how to work in a team

